%{
#include <stdio.h>
#include <string.h>
#include <math.h>

struct TSItem {
	int index;
	char token[20];
};

struct FIPItem {
	int code;
	int index;
	char token[20];
};

struct FIPItem fip[512];
int fip_count = 0;
struct TSItem ts_consts[30];
int ts_conts_count = 1;
struct TSItem ts_identifiers[30];
int ts_ident_count = 1;

void addFip(int code, int index, char token[]);
void addConstant(int code, char token[]);
void addIdentifier(int code, char token[]);
int getCode(char token[]);

#define GRN   "\033[92m"
#define RESET "\x1B[0m"

FILE* fp;
char buf[1024];
%}

DIGIT   [0-9]
ID      [a-z][a-z0-9]*
OPERATORS    "+"|"-"|"*"|"/"|"="|"!="|"<"|">"
SEPARATORS	":"|"("|")"
KEYWORDS	"input"|"print"|"while"|"if"|"else"|"int"|"float"

%%
{OPERATORS}|{SEPARATORS}|{KEYWORDS} { addFip(getCode(yytext), 0, yytext); }
{DIGIT}+"."{DIGIT}*|{DIGIT}+     { addConstant(getCode("constant"), yytext); }
{ID}        { addIdentifier(getCode("identifier"), yytext); }
[ \t\n]+     /* whitespaces */
.           { printf("Unrecognized: %s\n", yytext);}
%%

void addFip(int code, int index, char token[]) {
    fip[fip_count].code = code;
    fip[fip_count].index = index;
    strcpy(fip[fip_count].token, token);
    fip_count += 1;
}

void addConstant(int code, char token[]) {
    for (int i = 0; i < ts_conts_count; ++i) {
        if (strcmp(ts_consts[i].token, token) == 0) {
            addFip(code, ts_consts[i].index, token);
            return;
        }
    }
    
    ts_consts[ts_conts_count].index = ts_conts_count;
    strcpy(ts_consts[ts_conts_count].token, token);
    ts_conts_count += 1;

    addFip(code, ts_consts[ts_conts_count - 1].index, token);
}

void addIdentifier(int code, char token[]) {
    for (int i = 0; i < ts_ident_count; ++i) {
        if (strcmp(ts_identifiers[i].token, token) == 0) {
            addFip(code, ts_identifiers[i].index, token);
            return;
        }
    }

    ts_identifiers[ts_ident_count].index = ts_ident_count;
    strcpy(ts_identifiers[ts_ident_count].token, token);
    ts_ident_count += 1;

    addFip(code, ts_identifiers[ts_ident_count - 1].index, token);
}

int getCode(char token[]) {
    if ((fp = fopen("table.txt", "r")) == NULL) {
        printf("eroare");
        return -1;
    }

    while (fgets(buf, 1024, fp) != NULL) {
        char *name = strtok(buf, " ");
        char *value = strtok(NULL, " ");
//        printf("%s -> %s", name, value);
        if (strcmp(token, name) == 0) {
            return atoi(value);
        }

    }

    return -1;
}

int main(int argc, char *argv[]) {
	if (argc!=2) {
		printf("Usage: <./a.out> <source file> \n");
		exit(0);
	}
	yyin = fopen(argv[1], "r");
	yylex();

	printf("----------------------------\n");
	printf(GRN"Program Internal Form\n"RESET);
	printf("Code\tIndex\tToken\n");
	for (int i = 0; i < fip_count; i++) {
		printf("%d\t%d\t%s\n", fip[i].code, fip[i].index, fip[i].token);
	}

	printf("\n----------------------------\n");
	printf(GRN"Symbol Table - Constants\n"RESET);
	printf("Index\tToken\n");
	for (int i = 1; i < ts_conts_count; i++) {
		printf("%d\t%s\n", ts_consts[i].index, ts_consts[i].token);
	}

	printf("\n----------------------------\n");
	printf(GRN"Symbol Table - Identifiers\n"RESET);
	printf("Index\tToken\n");
	for (int i = 1; i < ts_ident_count; i++) {
		printf("%d\t%s\n", ts_identifiers[i].index, ts_identifiers[i].token);
	}
}
